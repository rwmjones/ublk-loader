/* ublk-loader
 * Copyright (C) 2022 Red Hat Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of Red Hat nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <dlfcn.h>

#include "ublk-loader.h"

void *handle;
int thread_model;

static struct nbdkit_plugin plugin;
static void *dl;

static void plugin_register (const char *filename, void *dl,
                             struct nbdkit_plugin *(*plugin_init) (void));

/* Is it a plugin or filter name relative to the plugindir/filterdir? */
static inline bool
is_short_name (const char *filename)
{
  return strchr (filename, '.') == NULL && strchr (filename, '/') == NULL;
}

void
plugin_load (void)
{
  char *filename = (char *) plugin_name;
  bool free_filename = false;
  struct nbdkit_plugin *(*plugin_init) (void);
  char *error;

  if (is_short_name (plugin_name)) {
    /* Short names are rewritten relative to the plugindir. */
    if (asprintf (&filename,
                  "%s/nbdkit-%s-plugin.so",
                  NBDKIT_PLUGINDIR, plugin_name) == -1) {
      nbdkit_error ("asprintf: %m");
      exit (EXIT_FAILURE);
    }
    free_filename = true;
  }

  dl = dlopen (filename, RTLD_NOW|RTLD_GLOBAL);
  if (dl == NULL) {
    nbdkit_error ("error: cannot open nbdkit plugin '%s': %s",
                  plugin_name, dlerror ());
    exit (EXIT_FAILURE);
  }

  dlerror ();
  plugin_init = dlsym (dl, "plugin_init");
  if ((error = dlerror ()) != NULL) {
    nbdkit_error ("%s: %s", plugin_name, error);
    exit (EXIT_FAILURE);
  }
  if (!plugin_init) {
    nbdkit_error ("invalid plugin_init");
    exit (EXIT_FAILURE);
  }

  plugin_register (filename, dl, plugin_init);

  /* Call the plugin's .load method, if present. */
  if (plugin.load)
    plugin.load ();

  if (free_filename)
    free (filename);
}

static void
plugin_register (const char *filename, void *dl,
                 struct nbdkit_plugin *(*plugin_init) (void))
{
  const struct nbdkit_plugin *p;
  size_t size;

  /* Call the initialization function which will return the address of
   * the plugin's own struct nbdkit_plugin.
   */
  p = plugin_init ();
  if (p == NULL) {
    nbdkit_error ("plugin registration function failed");
    exit (EXIT_FAILURE);
  }

  /* Check it is API compatible. */
  if (p->_api_version < 0 || p->_api_version > 2) {
    nbdkit_error ("plugin is incopatible with this version of %s "
                  "(_api_version = %d)",
                  "ublk-loader", p->_api_version);
    exit (EXIT_FAILURE);
  }

  /* The plugin might be older or newer than ublk-loader so only copy
   * up to the self-declared _struct_size and zero out the rest.
   */
  size = sizeof plugin;      /* our struct */
  memset (&plugin, 0, size); /* we don't need to do this, but do it anyway */
  if (size > p->_struct_size)
    size = p->_struct_size;
  memcpy (&plugin, p, size);

  /* Some fields in the plugin must exist. */
  if (plugin.open == NULL) {
    nbdkit_error ("plugin must have a .open callback");
    exit (EXIT_FAILURE);
  }
  if (plugin.get_size == NULL) {
    nbdkit_error ("plugin must have a .get_size callback");
    exit (EXIT_FAILURE);
  }
  if (plugin.pread == NULL && plugin._pread_v1 == NULL) {
    nbdkit_error ("plugin must have a .pread callback");
    exit (EXIT_FAILURE);
  }
}

void
plugin_unload (void)
{
  if (plugin.unload)
    plugin.unload ();
  dlclose (dl);
}

const char *
plugin_magic_config_key (void)
{
  return plugin.magic_config_key;
}

void
plugin_config (const char *key, const char *value)
{
  if (!plugin.config) {
    nbdkit_error ("this plugin does not need command line configuration");
    exit (EXIT_FAILURE);
  }

  if (plugin.config (key, value) == -1)
    exit (EXIT_FAILURE);
}

void
plugin_config_complete (void)
{
  if (!plugin.config_complete)
    return;
  if (plugin.config_complete () == -1)
    exit (EXIT_FAILURE);
}

void
plugin_lock_thread_model (void)
{
  if (plugin.thread_model)
    thread_model = plugin.thread_model ();
  else
    thread_model = plugin._thread_model;
}

void
plugin_get_ready (void)
{
  if (!plugin.get_ready)
    return;
  if (plugin.get_ready () == -1)
    exit (EXIT_FAILURE);
}

void
plugin_after_fork (void)
{
  if (!plugin.after_fork)
    return;
  if (plugin.after_fork () == -1)
    exit (EXIT_FAILURE);
}

void
plugin_cleanup (void)
{
  if (plugin.cleanup)
    plugin.cleanup ();
}

int
plugin_open (int readonly)
{
  // XXX
  // set handle
  return -1;
}

int
plugin_pread (void *handle, void *buf, uint32_t count,
              uint64_t offset)
{
  //XXX
  return -1;
}

int
plugin_pwrite (void *handle, const void *buf, uint32_t count,
               uint64_t offset, uint32_t flags)
{
  //XXX
  return -1;
}

int
plugin_flush (void *handle)
{
  //XXX
  return -1;
}

int
plugin_trim (void *handle, uint32_t count,
             uint64_t offset, uint32_t flags)
{
  //XXX
  return -1;
}

int
plugin_zero (void *handle, uint32_t count,
             uint64_t offset, uint32_t flags)
{
  //XXX
  return -1;
}

int
plugin_get_error (void)
{
  //XXX
  return EINVAL;
}
