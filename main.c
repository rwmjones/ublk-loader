/* ublk-loader
 * Copyright (C) 2022 Red Hat Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of Red Hat nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <signal.h>
#include <getopt.h>

#include <ublksrv.h>

#include "ispowerof2.h"
#include "string-vector.h"
#include "vector.h"

#include "ublk-loader.h"

#define DEVICE_PREFIX "/dev/ublkb"
#define DEVICE_PREFIX_LEN 10

char *device_file;
char *plugin_name;
unsigned nr_threads = 4;
bool readonly = false;
bool rotational;
bool can_fua;
bool can_flush;
bool can_trim;
bool can_zero;
uint64_t size;
uint64_t min_block_size;
uint64_t pref_block_size;
bool verbose = false;
bool configured = false;

/* The single control device.  This is a global so the signal handler
 * can attempt to stop the device.
 */
static struct ublksrv_ctrl_dev *dev;

static bool is_config_key (const char *key, size_t len);

static void __attribute__((noreturn))
usage (FILE *fp, int exitcode)
{
  fprintf (fp,
"\n"
"Load nbdkit plugins as Linux userspace block devices:\n"
"\n"
"    ublk-loader " DEVICE_PREFIX "<N> PLUGIN ARGS ...\n"
"\n"
"You can also use just the device number or '-' to allocate one:\n"
"\n"
"    ublk-loader <N> ...\n"
"    ublk-loader - ...\n"
"\n"
"To unmount:\n"
"\n"
"    ublk del -n <N>\n"
"\n"
"Other options:\n"
"\n"
"    ublk-loader --help\n"
"    ublk-loader --version\n"
"\n"
"Please read the ublk-loader(8) manual page for full usage.\n"
"\n"
);
  exit (exitcode);
}

static void signal_handler (int sig);

int
main (int argc, char *argv[])
{
  enum {
    HELP_OPTION = CHAR_MAX + 1,
    LONG_OPTIONS,
    SHORT_OPTIONS,
    VERSION_OPTION,
  };
  const char *short_options = "rv";
  const struct option long_options[] = {
    { "help",               no_argument,       NULL, HELP_OPTION },
    { "long-options",       no_argument,       NULL, LONG_OPTIONS },
    { "readonly",           no_argument,       NULL, 'r' },
    { "read-only",          no_argument,       NULL, 'r' },
    { "short-options",      no_argument,       NULL, SHORT_OPTIONS },
    { "verbose",            no_argument,       NULL, 'v' },
    { "version",            no_argument,       NULL, VERSION_OPTION },

    { NULL }
  };
  int c, r;
  size_t i;
  int64_t rs;
  uint64_t max_block_size;
  const char *s;
  struct ublksrv_dev_data data = { .dev_id = -1 };
  struct sigaction sa = { 0 };
  static const char *magic_config_key;

  for (;;) {
    c = getopt_long (argc, argv, short_options, long_options, NULL);
    if (c == -1)
      break;

    switch (c) {
    case HELP_OPTION:
      usage (stdout, EXIT_SUCCESS);

    case LONG_OPTIONS:
      for (i = 0; long_options[i].name != NULL; ++i) {
        if (strcmp (long_options[i].name, "long-options") != 0 &&
            strcmp (long_options[i].name, "short-options") != 0)
          printf ("--%s\n", long_options[i].name);
      }
      exit (EXIT_SUCCESS);

    case SHORT_OPTIONS:
      for (i = 0; short_options[i]; ++i) {
        if (short_options[i] != ':' && short_options[i] != '+')
          printf ("-%c\n", short_options[i]);
      }
      exit (EXIT_SUCCESS);

    case 'r':
      readonly = true;
      break;

    case 'v':
      verbose = true;
      break;

    case VERSION_OPTION:
      printf ("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
      fflush (stdout);
      exit (EXIT_SUCCESS);

    default:
      usage (stderr, EXIT_FAILURE);
    }
  }

  /* There must be at least 2 parameters (device and plugin name). */
  if (argc - optind < 2)
    usage (stderr, EXIT_FAILURE);

  /* Parse and check the device name. */
  s = argv[optind++];
  /* /dev/ublkc<N> */
  if (strncmp (s, DEVICE_PREFIX, DEVICE_PREFIX_LEN) == 0) {
    if (sscanf (&s[DEVICE_PREFIX_LEN], "%u", &data.dev_id) != 1) {
      fprintf (stderr, "%s: could not parse ublk device name: %s\n",
               argv[0], s);
      exit (EXIT_FAILURE);
    }
  }
  else if (s[0] >= '0' && s[0] <= '9') {
    if (sscanf (s, "%u", &data.dev_id) != 1) {
      fprintf (stderr, "%s: could not parse ublk device name: %s\n",
               argv[0], s);
      exit (EXIT_FAILURE);
    }
  }
  else if (s[0] == '-') {
    data.dev_id = -1;           /* autoallocate */
  }
  else {
    fprintf (stderr, "%s: expecting device name %s<N>\n",
             argv[0], DEVICE_PREFIX);
    exit (EXIT_FAILURE);
  }

  /* The next parameter is the plugin name. */
  plugin_name = argv[optind++];

  /* Load the plugin. */
  plugin_load ();

  /* Read the command line. */
  magic_config_key = plugin_magic_config_key ();

  for (i = 0; optind < argc; ++i, ++optind) {
    const char *p;
    size_t n;

    p = strchr (argv[optind], '=');
    n = p - argv[optind];

    if (p && is_config_key (argv[optind], n)) { /* key=value? */
      const char *key = nbdkit_strndup_intern (argv[optind], n);
      plugin_config (key, p+1);
    }
    else if (magic_config_key == NULL) {
      if (i == 0)               /* magic script parameter */
        plugin_config ("script", argv[optind]);
      else {
        fprintf (stderr,
                 "%s: expecting key=value on the command line but got: %s\n",
                 "ublk-loader", argv[optind]);
        exit (EXIT_FAILURE);
      }
    }
    else {
      plugin_config (magic_config_key, argv[optind]);
    }
  }

  plugin_config_complete ();
  configured = true;

  /* Select the thread model. */
  plugin_lock_thread_model ();
  debug ("thread_model = %d", thread_model);

  /* Start serving. */
  plugin_get_ready ();
  plugin_after_fork ();

#if 0
  /* Get the size and preferred block sizes. */
  rs = nbd_get_size (nbd.ptr[0]);
  if (rs == -1) {
    fprintf (stderr, "%s\n", nbd_get_error ());
    exit (EXIT_FAILURE);
  }
  size = (uint64_t) rs;

  rs = nbd_get_block_size (nbd.ptr[0], LIBNBD_SIZE_MAXIMUM);
  if (rs <= 0 || rs > 64 * 1024 * 1024)
    max_block_size = 64 * 1024 * 1024;
  else
    max_block_size = rs;
  if (!is_power_of_2 (max_block_size)) {
    fprintf (stderr,
             "%s: %s block size is not a power of two: %" PRIu64 "\n",
             argv[0], "maximum", max_block_size);
    exit (EXIT_FAILURE);
  }

  rs = nbd_get_block_size (nbd.ptr[0], LIBNBD_SIZE_PREFERRED);
  if (rs <= 0)
    pref_block_size = 4096;
  else
    pref_block_size = rs;
  if (!is_power_of_2 (pref_block_size)) {
    fprintf (stderr,
             "%s: %s block size is not a power of two: %" PRIu64 "\n",
             argv[0], "preferred", pref_block_size);
    exit (EXIT_FAILURE);
  }

  rs = nbd_get_block_size (nbd.ptr[0], LIBNBD_SIZE_MINIMUM);
  if (rs <= 0)
    min_block_size = 512; /* minimum that the kernel supports */
  else
    min_block_size = rs;
  if (!is_power_of_2 (min_block_size)) {
    fprintf (stderr,
             "%s: %s block size is not a power of two: %" PRIu64 "\n",
             argv[0], "minimum", min_block_size);
    exit (EXIT_FAILURE);
  }

  /* If the remote NBD server is readonly, then act as if the '-r'
   * flag was given on the nbdublk command line.
   */
  if (nbd_is_read_only (nbd.ptr[0]) > 0)
    readonly = true;

  rotational = nbd_is_rotational (nbd.ptr[0]) > 0;
  can_fua = nbd_can_fua (nbd.ptr[0]) > 0;

  debug ("size: %" PRIu64 " connections: %u%s",
         size, connections, readonly ? " readonly" : "");

  /* Fill in other fields in 'data' struct. */
  data.max_io_buf_bytes = max_block_size;
  data.nr_hw_queues = connections;
  data.queue_depth = 64;
  data.tgt_type = "nbd";
  data.tgt_ops = &tgt_type;
  data.flags = 0;
  data.ublksrv_flags = UBLKSRV_F_NEED_EVENTFD;

  dev = ublksrv_ctrl_init (&data);
  if (!dev) {
    fprintf (stderr, "%s: ublksrv_ctrl_init: %m\n", argv[0]);
    exit (EXIT_FAILURE);
  }

  /* Register signal handlers to try to stop the device. */
  sa.sa_handler = signal_handler;
  sigaction (SIGHUP, &sa, NULL);
  sigaction (SIGINT, &sa, NULL);
  sigaction (SIGTERM, &sa, NULL);
  sa.sa_handler = SIG_IGN;
  sigaction (SIGPIPE, &sa, NULL);

  r = ublksrv_ctrl_add_dev (dev);
  if (r < 0) {
    errno = -r;
    fprintf (stderr, "%s: ublksrv_ctrl_add_dev: "DEVICE_PREFIX "%d: %m\n",
             argv[0], dev->dev_info.dev_id);
    ublksrv_ctrl_deinit (dev);
    exit (EXIT_FAILURE);
  }

  debug ("%s: created %s%d", DEVICE_PREFIX, dev->dev_info.dev_id);

  /* XXX nbdfuse creates a pid file.  However I reason that you can
   * tell if the service is available when the block device is created
   * so a pid file is not necessary.  May need to revisit this.
   */

  if (start_daemon (dev) == -1) {
    ublksrv_ctrl_del_dev (dev);
    ublksrv_ctrl_deinit (dev);
    for (i = 0; i < nbd.len; ++i)
      nbd_close (nbd.ptr[i]);
    exit (EXIT_FAILURE);
  }

  /* Close ublk device. */
  ublksrv_ctrl_del_dev (dev);
  ublksrv_ctrl_deinit (dev);

  /* Close NBD handle(s). */
  for (i = 0; i < nbd.len; ++i)
    nbd_close (nbd.ptr[i]);
#endif

  /* Unload the plugin before exit. */
  plugin_cleanup ();
  plugin_unload ();

  /* Free up interned keys. */
  free_interns ();

  exit (EXIT_SUCCESS);
}

static void
signal_handler (int sig)
{
  /* XXX Racy, but not much else we can do. */
  ublksrv_ctrl_stop_dev (dev);
}

/* When parsing plugin and filter config key=value from the command
 * line, is the key a simple alphanumeric with period, underscore or
 * dash?
 */
static bool
is_config_key (const char *key, size_t len)
{
  static const char allowed_first[] =
    "abcdefghijklmnopqrstuvwxyz"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  static const char allowed[] =
    "._-"
    "0123456789"
    "abcdefghijklmnopqrstuvwxyz"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  if (len == 0)
    return false;

  if (strchr (allowed_first, key[0]) == NULL)
    return false;

  /* This works in context of the caller since key[len] == '='. */
  if (strspn (key, allowed) != len)
    return false;

  return true;
}
