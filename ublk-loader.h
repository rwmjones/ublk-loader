/* ublk-loader
 * Copyright (C) 2022 Red Hat Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of Red Hat nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef UBLK_LOADER_H
#define UBLK_LOADER_H

#include <stdbool.h>
#include <stdarg.h>

#include <ublksrv.h>

#define NBDKIT_API_VERSION 2
#include <nbdkit-plugin.h>

#include "vector.h"

#define UBLKSRV_TGT_TYPE_NBDKIT 0

extern char *device_file;
extern char *plugin_name;
extern unsigned nr_threads;
extern bool readonly;
extern bool rotational;
extern bool can_fua;
extern bool can_flush;
extern bool can_trim;
extern bool can_zero;
extern uint64_t size;
extern uint64_t min_block_size;
extern uint64_t pref_block_size;
extern bool verbose;
extern bool configured;

extern struct ublksrv_tgt_type tgt_type;

extern void *handle;
extern int thread_model;

extern int start_daemon (struct ublksrv_ctrl_dev *dev);

extern void free_interns (void);

extern void plugin_load (void);
extern void plugin_unload (void);
extern const char *plugin_magic_config_key (void);
extern void plugin_config (const char *key, const char *value);
extern void plugin_config_complete (void);
extern void plugin_lock_thread_model (void);
extern void plugin_get_ready (void);
extern void plugin_after_fork (void);
extern void plugin_cleanup (void);

extern int plugin_open (int readonly);
extern int plugin_pread (void *handle, void *buf, uint32_t count,
                         uint64_t offset);
extern int plugin_pwrite (void *handle, const void *buf, uint32_t count,
                          uint64_t offset, uint32_t flags);
extern int plugin_flush (void *handle);
extern int plugin_trim (void *handle, uint32_t count,
                        uint64_t offset, uint32_t flags);
extern int plugin_zero (void *handle, uint32_t count,
                        uint64_t offset, uint32_t flags);
extern int plugin_get_error (void);

#define debug(fs, ...)                                   \
  do {                                                   \
    if (verbose)                                         \
      nbdkit_debug ((fs), ##__VA_ARGS__);                \
  } while (0)

#endif /* UBLK_LOADER_H */
