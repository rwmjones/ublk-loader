## Load nbdkit plugins as Linux userspace block devices

[nbdkit](https://gitlab.com/nbdkit/nbdkit) is a plugin-based Network
Block Device (NBD) server.  It makes it very easy to create Linux
block devices and export them over the NBD network protocol.  There is
[a large ecosystem of plugins for
nbdkit](https://libguestfs.org/nbdkit.1.html#Plugins).  New plugins
can be written in C, or many other programming and scripting
languages.

Linux ≥ 6.0 added a feature called
[ublk](https://lwn.net/Articles/903855/) that allows Linux block
devices to be created in userspace.

**ublk-loader** is a way to load most nbdkit plugins as Linux
userspace block devices.  It's the best of all worlds:

 * nbdkit's easy ways to build block devices
 * fast userspace block devices
 * reuse the same code for networked block devices via nbdkit

There are some shortcomings and unsupported features.  This software
is a bit experimental at the moment.

[For full documentation see the manual page.](ublk-loader.pod)

### Example usage

Using
[nbdkit-file-plugin(1)](https://libguestfs.org/nbdkit-file-plugin.1.html)
turn the local file (`disk.img`) into a userspace device called
`/dev/ublkb0`.  This is similar to loop mounting.

```
# ublk-loader /dev/ublkb0 file disk.img
```

Using
[nbdkit-memory-plugin(1)](https://libguestfs.org/nbdkit-memory-plugin.1.html)
create a sparse RAM disk called `/dev/ublkb0`:

```
# ublk-loader /dev/ublkb0 memory 1G
```

These examples load the plugin from the nbdkit plugins directory.  You
can give the filename or path of the plugin instead.  To find the name
of the plugins directory use:

```
$ nbdkit --dump-config | grep ^plugindir=
plugindir=/usr/lib64/nbdkit/plugins
```

## License

This software is copyright (C) Red Hat Inc. and licensed under a BSD
license.  See [LICENSE](LICENSE) for details.

## Building from source

### Requirements

* Linux ≥ 6.0
* GCC or Clang
* bash
* GNU make
* [ubdsrv](https://github.com/ming1/ubdsrv) ≥ 1.0
* [nbdkit](https://gitlab.com/nbdkit/nbdkit)

### Optional

Perl `Pod::Man` and `Pod::Simple` can be installed if you want to
build the documentation (recommended).

### Building

```
To build from tarball:         To build from git:

                               autoreconf -i
./configure                    ./configure
make                           make
make check                     make check
```
