/* ublk-loader
 * Copyright (C) 2022 Red Hat Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of Red Hat nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <poll.h>
#include <errno.h>
#include <pthread.h>

#ifdef HAVE_STDATOMIC_H
#include <stdatomic.h>
#else
/* Rely on ints being atomic enough on the platform. */
#define _Atomic /**/
#endif

#include <ublksrv.h>
#include <ublksrv_aio.h>

#include "ispowerof2.h"
#include "vector.h"

#include "ublk-loader.h"

/* Thread model:
 *
 * There are 2 * 'nr_threads' threads created.
 *
 * For each pair of threads, one thread ('ublk_thread') handles a
 * single queue of io_uring traffic from ublk.  The second thread
 * ('plugin_thread') makes calls into the plugin.  A single 'struct
 * thread_info' is shared between these two threads.
 *
 * If the thread model is more restrictive than
 * NBDKIT_THREAD_MODEL_PARALLEL then locks are acquired as necessary.
 *
 * There is only one connection open into the nbdkit plugin.
 *
 * The use of the ublk AIO request API is useless right now.  But we
 * could in future allow there to be more nbdkit threads than ublk
 * threads, since a single ublk queue can handle multiple requests and
 * could hand those off to waiting nbdkit threads.
 */

struct thread_info {
  struct ublksrv_dev *dev;
  size_t i;                     /* index into nbd.ptr[], also q_id */
  pthread_t ublk_thread;
  pthread_t plugin_thread;

  struct ublksrv_aio_ctx *aio_ctx;
  struct ublksrv_aio_list compl;
};
DEFINE_VECTOR_TYPE(thread_infos, struct thread_info)
static thread_infos thread_info;

static pthread_barrier_t barrier;

static char jbuf[4096];
static pthread_mutex_t jbuf_lock = PTHREAD_MUTEX_INITIALIZER;

/* Single from the plugin thread to the ublk thread that the current
 * command has completed.
 */
static void
command_completed (struct ublksrv_aio *req, int err)
{
  int q_id = ublksrv_aio_qid (req->id);
  struct ublksrv_aio_list *compl = &thread_info.ptr[q_id].compl;

  debug ("command_completed: tag=%d q_id=%u error=%d",
         ublksrv_aio_tag (req->id), ublksrv_aio_qid (req->id), err);

  /* If the command failed, override the normal result. */
  if (err != 0)
    req->res = err;

  pthread_spin_lock (&compl->lock);
  aio_list_add (&compl->list, req);
  pthread_spin_unlock (&compl->lock);
}

static int
aio_submitter (struct ublksrv_aio_ctx *ctx, struct ublksrv_aio *req)
{
  const struct ublksrv_io_desc *iod = &req->io;
  const unsigned op = ublksrv_get_op (iod);
  const unsigned flags = ublksrv_get_flags (iod);
  const bool fua = flags & UBLK_IO_F_FUA;
  const bool alloc_zero = flags & UBLK_IO_F_NOUNMAP; /* else punch hole */
  const size_t q_id = ublksrv_aio_qid (req->id); /* also NBD handle number */
  uint32_t nbdkit_flags = 0;
  int err = 0;
  int64_t r;

  debug ("handle_io_async: tag = %d q_id = %zu",
         ublksrv_aio_tag (req->id), q_id);

  req->res = iod->nr_sectors << 9;

  switch (op) {
  case UBLK_IO_OP_READ:
    if (plugin_pread (handle, (void *) iod->addr,
                      iod->nr_sectors << 9, iod->start_sector << 9) == -1) {
      err = plugin_get_error ();
      fprintf (stderr, "%s: %s: %s\n", "ublk-loader", "pread", strerror (err));
    }
    break;

  case UBLK_IO_OP_WRITE:
    if (fua && can_fua)
      nbdkit_flags |= NBDKIT_FLAG_FUA;

    if (plugin_pwrite (handle, (const void *) iod->addr,
                       iod->nr_sectors << 9, iod->start_sector << 9,
                       nbdkit_flags) == -1) {
      err = plugin_get_error ();
      fprintf (stderr, "%s: %s: %s\n", "ublk-loader", "pwrite", strerror (err));
    }
    break;

  case UBLK_IO_OP_FLUSH:
    if (can_flush &&
        plugin_flush (handle) == -1) {
      err = plugin_get_error ();
      fprintf (stderr, "%s: %s: %s\n", "ublk-loader", "flush",
               strerror (err));
    }
    break;

  case UBLK_IO_OP_DISCARD:
    if (fua && can_fua)
      nbdkit_flags |= NBDKIT_FLAG_FUA;

    if (can_trim &&
        plugin_trim (handle,
                     iod->nr_sectors << 9, iod->start_sector << 9,
                     nbdkit_flags) == -1) {
      err = plugin_get_error ();
      fprintf (stderr, "%s: %s: %s\n", "ublk-loader", "flush",
               strerror (err));
    }
    break;

  case UBLK_IO_OP_WRITE_ZEROES:
    if (fua && can_fua)
      nbdkit_flags |= NBDKIT_FLAG_FUA;

    if (!alloc_zero)
      nbdkit_flags |= NBDKIT_FLAG_MAY_TRIM;

    if (can_zero &&
        plugin_zero (handle,
                     iod->nr_sectors << 9, iod->start_sector << 9,
                     nbdkit_flags) == -1) {
      err = plugin_get_error ();
      fprintf (stderr, "%s: %s: %s\n", "ublk-loader", "flush",
               strerror (err));
    }
    break;

  default:
    fprintf (stderr, "%s: unknown operation %u\n", "ublk-loader", op);
    err = -ENOTSUP;
  }

  command_completed (req, err);

  /* XXX If we were going to dispatch commands to multiple threads
   * above, then we would return 0 here as the request was not
   * completed.  However in the current code we always return 1
   * because the request has always been completed.
   */
  return 1;
}

static void *
plugin_thread (void *vpinfo)
{
  struct thread_info *ti = vpinfo;
  struct ublksrv_aio_ctx *aio_ctx = thread_info.ptr[ti->i].aio_ctx;
  struct ublksrv_aio_list *c = &thread_info.ptr[ti->i].compl;

  /* Signal to the main thread that we have initialized. */
  pthread_barrier_wait (&barrier);

  while (!ublksrv_aio_ctx_dead (aio_ctx)) {
    struct aio_list compl;
    struct pollfd fds[1];

    aio_list_init (&compl);
    ublksrv_aio_submit_worker (aio_ctx, aio_submitter, &compl);

    pthread_spin_lock (&c->lock);
    aio_list_splice (&c->list, &compl);
    pthread_spin_unlock (&c->lock);

    ublksrv_aio_complete_worker (aio_ctx, &compl);

    fds[1].fd = aio_ctx->efd;
    fds[1].events = POLLIN;
    if (poll (fds, 1, -1) == -1) {
      perror ("ublk-loader: poll");
      exit (EXIT_FAILURE);
    }
  }

  return NULL;
}

static void *
ublk_thread (void *vpinfo)
{
  struct thread_info *thread_info = vpinfo;
  struct ublksrv_dev *dev = thread_info->dev;
  const unsigned dev_id = dev->ctrl_dev->dev_info.dev_id;
  const size_t q_id = thread_info->i;
  struct ublksrv_queue *q;
  int r;

  pthread_mutex_lock (&jbuf_lock);
  ublksrv_json_write_queue_info (dev->ctrl_dev, jbuf, sizeof jbuf,
                                 q_id, gettid ());
  pthread_mutex_unlock (&jbuf_lock);

  q = ublksrv_queue_init (dev, q_id, NULL);
  if (!q) {
    perror ("ublksrv_queue_init");
    return NULL;
  }

  debug ("ublk tid %d dev %d queue %d started", q->tid, dev_id, q->q_id);

  for (;;) {
    r = ublksrv_process_io (q);
    if (r < 0) {
      if (r != -ENODEV) { /* ENODEV is expected when the device is deleted */
        errno = -r;
        perror ("ublksrv_process_io");
      }
      break;
    }
  }

  debug ("ublk tid %d dev %d queue %d exited", q->tid, dev_id, q->q_id);

  ublksrv_queue_deinit (q);
  return NULL;
}

static int
set_parameters (struct ublksrv_ctrl_dev *ctrl_dev,
                const struct ublksrv_dev *dev)
{
  struct ublksrv_ctrl_dev_info *dinfo = &ctrl_dev->dev_info;
  const unsigned attrs =
    (readonly ? UBLK_ATTR_READ_ONLY : 0) |
    (rotational ? UBLK_ATTR_ROTATIONAL : 0) |
    (can_fua ? UBLK_ATTR_FUA : 0);
  struct ublk_params p = {
    .types = UBLK_PARAM_TYPE_BASIC,
    .basic = {
      .attrs                = attrs,
      .logical_bs_shift     = 9,
      .physical_bs_shift    = 9,
      .io_opt_shift         = log_2_bits (pref_block_size),
      .io_min_shift         = log_2_bits (min_block_size),
      .max_sectors          = dinfo->max_io_buf_bytes >> 9,
      .dev_sectors          = dev->tgt.dev_size >> 9,
    },
    .discard = {
      .max_discard_sectors  = UINT_MAX >> 9,
      .max_discard_segments = 1,
    },
  };
  int r;

  pthread_mutex_lock (&jbuf_lock);
  ublksrv_json_write_params (&p, jbuf, sizeof jbuf);
  pthread_mutex_unlock (&jbuf_lock);

  r = ublksrv_ctrl_set_params (ctrl_dev, &p);
  if (r < 0) {
    errno = -r;
    perror ("ublksrv_ctrl_set_params");
    return -1;
  }

  return 0;
}

int
start_daemon (struct ublksrv_ctrl_dev *ctrl_dev)
{
  const struct ublksrv_ctrl_dev_info *dinfo = &ctrl_dev->dev_info;
  struct ublksrv_dev *dev;
  size_t i;
  int r;

  assert (dinfo->nr_hw_queues == nr_threads);

  debug ("starting daemon");

  /* This barrier is used to ensure all plugin threads have started up
   * before we proceed to start the device.
   */
  r = pthread_barrier_init (&barrier, NULL, nr_threads + 1);
  if (r != 0) {
    errno = r;
    perror ("ublk-loader: pthread_barrier_init");
    return -1;
  }

  /* Reserve space for the thread_info. */
  if (thread_infos_reserve (&thread_info, nr_threads) == -1) {
    perror ("ublk-loader: realloc");
    return -1;
  }

  r = ublksrv_ctrl_get_affinity (ctrl_dev);
  if (r < 0) {
    errno = r;
    perror ("ublk-loader: ublksrv_ctrl_get_affinity");
    return -1;
  }

  dev = ublksrv_dev_init (ctrl_dev);
  if (!dev) {
    /* Annoyingly libublksrv logs some not very useful information to
     * syslog when this fails.
     */
    fprintf (stderr, "%s: ublksrv_dev_init failed: "
             "there may be more information in syslog\n",
             "ublk-loader");
    return -1;
  }

  /* Create the threads. */
  for (i = 0; i < nr_threads; ++i) {
    /* Note this cannot fail because of previous reserve. */
    thread_infos_append (&thread_info,
                         (struct thread_info)
                         { .dev = dev, .i = i,});

    thread_info.ptr[i].aio_ctx = ublksrv_aio_ctx_init (dev, 0);
    if (!thread_info.ptr[i].aio_ctx) {
      perror ("ublk-loader: ublksrv_aio_ctx_init");
      return -1;
    }
    ublksrv_aio_init_list (&thread_info.ptr[i].compl);

    r = pthread_create (&thread_info.ptr[i].ublk_thread, NULL,
                        ublk_thread, &thread_info.ptr[i]);
    if (r != 0)
      goto bad_pthread;
    r = pthread_create (&thread_info.ptr[i].plugin_thread, NULL,
                        plugin_thread, &thread_info.ptr[i]);
    if (r != 0) {
    bad_pthread:
      errno = r;
      perror ("ublk-loader: pthread");
      ublksrv_dev_deinit (dev);
      return -1;
    }
  }

  /* Wait on the barrier to ensure all plugin threads are up. */
  pthread_barrier_wait (&barrier);
  pthread_barrier_destroy (&barrier);

  if (set_parameters (ctrl_dev, dev) == -1) {
    ublksrv_dev_deinit (dev);
    return -1;
  }

  /* Start the device. */
  r = ublksrv_ctrl_start_dev (ctrl_dev, getpid ());
  if (r < 0) {
    errno = -r;
    perror ("ublk-loader: ublksrv_ctrl_start_dev");
    ublksrv_dev_deinit (dev);
    return -1;
  }

  ublksrv_ctrl_get_info (ctrl_dev);
  ublksrv_ctrl_dump (ctrl_dev, jbuf);

  /* Wait for io_uring threads to exit. */
  for (i = 0; i < nr_threads; ++i)
    pthread_join (thread_info.ptr[i].ublk_thread, NULL);

  for (i = 0; i < nr_threads; ++i) {
    ublksrv_aio_ctx_shutdown (thread_info.ptr[i].aio_ctx);
    pthread_join (thread_info.ptr[i].plugin_thread, NULL);
    ublksrv_aio_ctx_deinit (thread_info.ptr[i].aio_ctx);
  }

  ublksrv_dev_deinit (dev);
  //thread_infos_reset (&thread_info);
  return 0;
}

static int
init_tgt (struct ublksrv_dev *dev, int type, int argc, char *argv[])
{
  const struct ublksrv_ctrl_dev_info  *info = &dev->ctrl_dev->dev_info;
  struct ublksrv_tgt_info *tgt = &dev->tgt;
  struct ublksrv_tgt_base_json tgt_json = {
    .type = type,
    .name = "nbdkit",
  };

  debug ("init_tgt: type = %d", type);

  if (type != UBLKSRV_TGT_TYPE_NBDKIT)
    return -1;

  tgt_json.dev_size = tgt->dev_size = size;
  tgt->tgt_ring_depth = info->queue_depth;
  tgt->nr_fds = 0;

  ublksrv_json_write_dev_info (dev->ctrl_dev, jbuf, sizeof jbuf);
  ublksrv_json_write_target_base_info (jbuf, sizeof jbuf, &tgt_json);

  return 0;
}

static void
handle_event (struct ublksrv_queue *q)
{
  struct ublksrv_aio_ctx *aio_ctx = thread_info.ptr[q->q_id].aio_ctx;

  debug ("handle_event: q_id = %d", q->q_id);
  ublksrv_aio_handle_event (aio_ctx, q);
}

static int
handle_io_async (struct ublksrv_queue *q, int tag)
{
  struct ublksrv_aio_ctx *aio_ctx = thread_info.ptr[q->q_id].aio_ctx;
  const struct ublksrv_io_desc *iod = ublksrv_get_iod (q, tag);
  struct ublksrv_aio *req = ublksrv_aio_alloc_req (aio_ctx, 0);

  req->io = *iod;
  req->id = ublksrv_aio_pid_tag (q->q_id, tag);
  debug ("qid %d tag %d", q->q_id, tag);
  ublksrv_aio_submit_req (aio_ctx, q, req);

  return 0;
}

struct ublksrv_tgt_type tgt_type = {
  .type = UBLKSRV_TGT_TYPE_NBDKIT,
  .name = "nbdkit",
  .init_tgt = init_tgt,
  .handle_io_async = handle_io_async,
  .handle_event = handle_event,
};
